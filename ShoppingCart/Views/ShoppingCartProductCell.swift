//
//  ShoppingCartProductCell.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 3/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

protocol ShoppingCartProductCellDelegate: class {
    func productCell(_ productCell: ShoppingCartProductCell, didChangeQuantity quantity: Int)
}

class ShoppingCartProductCell: UITableViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    
    weak var delegate: ShoppingCartProductCellDelegate?

    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        self.delegate?.productCell(self, didChangeQuantity: Int(sender.value))
    }
}
