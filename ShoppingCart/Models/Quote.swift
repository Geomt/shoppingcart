//
//  Quote.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 4/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation

struct Quote {
    let name: String
    let value: Decimal
}
