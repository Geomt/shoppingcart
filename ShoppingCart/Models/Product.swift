//
//  Product.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 3/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation

struct Product {
    let name: String
    let imageName: String?
    let price: Decimal
    let sellUnit: String
    
    init(name: String, imageName: String?, price: Decimal, sellUnit: String) {
        self.name = name
        self.imageName = imageName
        self.price = price
        self.sellUnit = sellUnit
    }
}

extension Product: Equatable {
    static func ==(lhs: Product, rhs: Product) -> Bool {
        return lhs.name == rhs.name && lhs.price == rhs.price && lhs.sellUnit == rhs.sellUnit
    }
}
