//
//  RemoteQuoteProvider.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 10/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation

protocol QuoteProvider {
    func fetchData(completionHandler: @escaping ([Quote], QuoteError?) -> Void)
}

enum QuoteError: Error {
    case network(error: Error)
    case jsonConversion(error: Error)
}

struct RemoteQuoteProvider: QuoteProvider {
    let apiKey = "7304bef242696a72e887b5b57095594c"
    let endpoint = "http://apilayer.net/api/live"
    
    func fetchData(completionHandler: @escaping ([Quote], QuoteError?) -> Void) {
        let urlComp = NSURLComponents(string: self.endpoint)!
        
        urlComp.queryItems = [URLQueryItem(name: "access_key", value: self.apiKey)]
        
        var urlRequest = URLRequest(url: urlComp.url!)
        urlRequest.httpMethod = "GET"
        let session = URLSession.shared
        
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            
            if let error = error {
                OperationQueue.main.addOperation({
                    completionHandler([], QuoteError.network(error: error))
                })
            } else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String : AnyObject]
                    
                    if let quotes = json["quotes"] as? [String: NSNumber] {
                        var quoteList: [Quote] = []
                        for (key, value) in quotes {
                            
                            quoteList.append(Quote(name: key.removeUSDPrefix(), value: value.decimalValue))
                            
                        }
                        
                        OperationQueue.main.addOperation({
                            completionHandler(quoteList, nil)
                        })
                    }
                } catch let error as NSError {
                    OperationQueue.main.addOperation({
                        completionHandler([], QuoteError.jsonConversion(error: error))
                    })
                }
            }
        })
        task.resume()
    }
}
