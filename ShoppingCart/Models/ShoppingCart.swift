//
//  ShoppingCart.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 10/7/17.
//  Copyright © 2017 None. All rights reserved.
//

typealias ProductInfo = (product: Product, quantity: Int)

struct ShoppingCart {
    var contents: [ProductInfo] = []
    
    init(productList: [Product]) {
        for product in productList {
            self.contents.append((product, 0))
        }
    }
}
