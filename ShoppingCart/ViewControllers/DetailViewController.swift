//
//  DetailViewController.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 3/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var quoteListViewModel: QuoteListViewModel!
    var totalAmountInDollars: Decimal = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.quoteListViewModel = QuoteListViewModel(quoteProvider: RemoteQuoteProvider(), defaultAmount: self.totalAmountInDollars, delegate: self)
        self.amountLabel.text = self.quoteListViewModel.mainTitle(for: nil)
    }
}

extension DetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.quoteListViewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.quoteListViewModel.numberOfQuotes
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuoteCell", for: indexPath)
        if let viewModel = self.quoteListViewModel.quoteViewModel(at: indexPath) {
            self.configureCell(cell: cell, with: viewModel)
        }
        return cell
    }
}

extension DetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let viewModel = self.quoteListViewModel.quoteViewModel(at: indexPath)
        self.amountLabel.text = self.quoteListViewModel.mainTitle(for: viewModel)
    }
}

// MARK:- Helper Methods
extension DetailViewController {
    func configureCell(cell: UITableViewCell, with viewModel: QuoteViewModel) {
        
        cell.textLabel?.text = viewModel.cellTitle
    }
}

extension DetailViewController: QuoteListViewModelDelegate {
    func quoteListViewModelDidUpdate(_ quoteListViewModel: QuoteListViewModel) {
        self.tableView.reloadData()
    }
}
