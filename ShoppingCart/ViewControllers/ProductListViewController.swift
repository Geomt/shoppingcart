//
//  ProductListViewController.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 3/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class ProductListViewController: UITableViewController {
    fileprivate var shoppingCartViewModel: ShoppingCartViewModel!
    
    override func viewDidLoad() {
        let productList = [Product(name: "Peas", imageName: "peas", price: 0.95, sellUnit: "bag"),
                    Product(name: "Eggs", imageName: "eggs", price: 2.1, sellUnit: "dozen"),
                    Product(name: "Milk", imageName: "milk", price: 1.3, sellUnit: "bottle"),
                    Product(name: "Beans", imageName: "beans", price: 0.73, sellUnit: "can")]
        self.shoppingCartViewModel = ShoppingCartViewModel(productList: productList)
        self.shoppingCartViewModel.delegate = self
        super.viewDidLoad()
    }
}

// MARK:- UITableViewDataSource
extension ProductListViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.shoppingCartViewModel.numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.shoppingCartViewModel.numberOfProducts
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingCartProductCell", for: indexPath) as! ShoppingCartProductCell
        if let viewModel = self.shoppingCartViewModel.productViewModel(at: indexPath) {
            self.configureCell(cell: cell, with: viewModel)
        }
        return cell
    }
    
}

// MARK:- Helper Methods
extension ProductListViewController {
    func configureCell(cell: ShoppingCartProductCell, with viewModel: ProductViewModel) {
        
        cell.titleLabel.text = viewModel.name
        cell.productImageView.image = viewModel.productImage
        cell.subtitleLabel.text = viewModel.subtitleMessage
        cell.quantityLabel.text = viewModel.quantity
        
        cell.delegate = self
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! DetailViewController
        vc.totalAmountInDollars = self.shoppingCartViewModel.totalAmount
    }
}

extension ProductListViewController: ShoppingCartProductCellDelegate {
    func productCell(_ productCell: ShoppingCartProductCell, didChangeQuantity quantity: Int) {
        
        guard let indexPath = self.tableView.indexPath(for: productCell) else {
            return
        }
        self.shoppingCartViewModel.updateQuantityForProduct(at: indexPath, with: quantity)
    }
}

extension ProductListViewController: ShoppingCartViewModelDelegate {
    func shoppingCartViewModelDidUpdate(_ shoppingCartViewModel: ShoppingCartViewModel) {
        self.tableView.reloadData()
    }
}
