//
//  ShoppingCartViewModel.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 3/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation

protocol ShoppingCartViewModelDelegate: class {
    func shoppingCartViewModelDidUpdate(_ shoppingCartViewModel: ShoppingCartViewModel)
}

class ShoppingCartViewModel {
    private var shoppingCart: ShoppingCart
    weak var delegate: ShoppingCartViewModelDelegate?
    
    required init(productList: [Product]) {
        self.shoppingCart = ShoppingCart(productList: productList)
    }
    
    var numberOfProducts: Int {
        return self.shoppingCart.contents.count
    }
    
    var numberOfSections: Int {
        return 1
    }
    
    var totalAmount: Decimal {
        let valuesList = self.shoppingCart.contents.map{$0.product.price * Decimal($0.quantity)}
        return valuesList.reduce(0.0, +)
    }
    
    func updateQuantityForProduct(at indexPath: IndexPath, with quantity: Int) {
        guard indexPath.row < self.numberOfProducts else {
            return
        }
        self.shoppingCart.contents[indexPath.row].quantity = quantity
        self.delegate?.shoppingCartViewModelDidUpdate(self)
    }
    
    func productViewModel(at indexPath: IndexPath) -> ProductViewModel? {
        guard indexPath.row < self.numberOfProducts else {
            return nil
        }
        return ProductViewModel(productInfo: self.shoppingCart.contents[indexPath.row])
    }
}
