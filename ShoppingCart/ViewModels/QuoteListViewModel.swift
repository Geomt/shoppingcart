//
//  QuoteListViewModel.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 4/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

protocol QuoteListViewModelDelegate: class {
    func quoteListViewModelDidUpdate(_ quoteListViewModel: QuoteListViewModel)
}

extension String {
    func removeUSDPrefix() -> String {
        return substring(from: index(startIndex, offsetBy: 3))
    }
}

class QuoteListViewModel {
    let defaultAmount: Decimal
    let quoteProvider: QuoteProvider
    
    var quoteList: [Quote] = []
    weak var delegate: QuoteListViewModelDelegate?
    
    init(quoteProvider: QuoteProvider, defaultAmount: Decimal, delegate: QuoteListViewModelDelegate?) {
        self.quoteProvider = quoteProvider
        self.defaultAmount = defaultAmount
        self.delegate = delegate
        
        self.quoteProvider.fetchData { (quoteList, error) in
            if let error = error {
                print(error as NSError)
                return
            }
            self.quoteList = quoteList
            self.delegate?.quoteListViewModelDidUpdate(self)
        }
    }
    
    var numberOfQuotes: Int {
        return self.quoteList.count
    }
    
    var numberOfSections: Int {
        return 1
    }
    
    func mainTitle(`for` viewModel: QuoteViewModel?) -> String {
        guard let viewModel = viewModel else {
            return "$: \(self.defaultAmount)"
        }
        return viewModel.mainTitle(baseAmount: self.defaultAmount)
    }
    
    func quoteViewModel(at indexPath: IndexPath) -> QuoteViewModel? {
        guard indexPath.row < self.numberOfQuotes else {
            return nil
        }
        let quote = self.quoteList[indexPath.row]
        return QuoteViewModel(quote: quote)
    }
}
