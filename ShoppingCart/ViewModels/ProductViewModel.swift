//
//  ProductViewModel.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 3/7/17.
//  Copyright © 2017 None. All rights reserved.
//
import UIKit

struct ProductViewModel {
    private let productInfo: ProductInfo
    
    init(productInfo: ProductInfo) {
        self.productInfo = productInfo
    }
    
    var name: String {
        return self.productInfo.product.name
    }
    
    var productImage: UIImage? {
        guard let imageName = self.productInfo.product.imageName else {
            return UIImage(named: "default")
        }
        return UIImage(named: imageName)
    }
    
    var subtitleMessage: String {
        return "$ \(self.productInfo.product.price) per \(self.productInfo.product.sellUnit)"
    }
    
    var quantity: String {
        return String(self.productInfo.quantity)
    }
}
