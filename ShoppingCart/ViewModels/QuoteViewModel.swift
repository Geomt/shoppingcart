//
//  QuoteViewModel.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 4/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

struct QuoteViewModel {
    private let quote: Quote
    
    init(quote: Quote) {
        self.quote = quote
    }
    
    var cellTitle: String {
        return self.symbolFrom(currencyCode: quote.name)
    }
    
    func mainTitle(baseAmount: Decimal) -> String {
        return "\(self.symbolFrom(currencyCode: quote.name)): \(quote.value * baseAmount)"
    }
    
    func symbolFrom(currencyCode: String) -> String {
        let localeComponents: [String: String] = [NSLocale.Key.currencyCode.rawValue: currencyCode]
        let localeIdentifier = NSLocale.localeIdentifier(fromComponents: localeComponents)
        let locale = NSLocale(localeIdentifier: localeIdentifier)
        // Apparently if the currency code is not recognizable it returns "¤". Couldn't find a case where nil was returned, but since Any? is returned I needed the default value
        return locale.object(forKey: NSLocale.Key.currencySymbol) as? String ?? "UNKNOWN"
    }
}
