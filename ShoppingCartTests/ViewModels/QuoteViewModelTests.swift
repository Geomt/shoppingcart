//
//  QuoteViewModelTests.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 10/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import XCTest

@testable import ShoppingCart

class QuoteViewModelTests: XCTestCase {
    
    func testEuroViewModel() {
        let quote = Quote(name: "EUR", value: 1.3)
        
        let quoteViewModel = QuoteViewModel(quote: quote)
        XCTAssertEqual(quoteViewModel.cellTitle, "€")
        XCTAssertEqual(quoteViewModel.mainTitle(baseAmount: 35), "€: 45.5")
    }
    
    func testSwissFrancViewModel() {
        let quote = Quote(name: "CHF", value: 1.03)
        
        let quoteViewModel = QuoteViewModel(quote: quote)
        XCTAssertEqual(quoteViewModel.cellTitle, "CHF")
        XCTAssertEqual(quoteViewModel.mainTitle(baseAmount: 27), "CHF: 27.81")
    }
    
    func testUnknownCodeViewModel() {
        let quote = Quote(name: "ZXCSD", value: 2)
        
        let quoteViewModel = QuoteViewModel(quote: quote)
        XCTAssertEqual(quoteViewModel.cellTitle, "¤")
        XCTAssertEqual(quoteViewModel.mainTitle(baseAmount: 5), "¤: 10")
    }
    
}
