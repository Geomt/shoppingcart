//
//  ProductViewModelTests.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 10/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import XCTest

@testable import ShoppingCart

class ProductViewModelTests: XCTestCase {
    
    func testViewModel() {
        let product = Product(name: "Eggs", imageName: "eggs", price: 5.3, sellUnit: "dozen")
        
        let productViewModel = ProductViewModel(productInfo: (product, 5))
        
        XCTAssertEqual(productViewModel.name, "Eggs")
        XCTAssertEqual(productViewModel.productImage, UIImage(named: "eggs"))
        XCTAssertEqual(productViewModel.subtitleMessage, "$ 5.3 per dozen")
        XCTAssertEqual(productViewModel.quantity, "5")
    }
    
    func testNoImageViewModel() {
        let noImageProduct = Product(name: "Apples", imageName: nil, price: 6.3, sellUnit: "piece")
        
        let noImageProductViewModel = ProductViewModel(productInfo: (noImageProduct, 10))
        
        XCTAssertEqual(noImageProductViewModel.name, "Apples")
        XCTAssertEqual(noImageProductViewModel.productImage, UIImage(named: "default"))
        XCTAssertEqual(noImageProductViewModel.subtitleMessage, "$ 6.3 per piece")
        XCTAssertEqual(noImageProductViewModel.quantity, "10")
    }
}
