//
//  ShoppingCartViewModelTests.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 10/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import XCTest

@testable import ShoppingCart

class ShoppingCartDelegateTester: ShoppingCartViewModelDelegate {
    var counter = 0
    
    func shoppingCartViewModelDidUpdate(_ shoppingCartViewModel: ShoppingCartViewModel) {
        counter += 1
    }
}

class ShoppingCartViewModelTests: XCTestCase {
    
    private var defaultProductList: [Product]!
    
    override func setUp() {
        self.defaultProductList = [Product(name: "Peas", imageName: "peas", price: 0.95, sellUnit: "bag"),
                                   Product(name: "Eggs", imageName: "eggs", price: 2.1, sellUnit: "dozen"),
                                   Product(name: "Milk", imageName: "milk", price: 1.3, sellUnit: "bottle"),
                                   Product(name: "Beans", imageName: "beans", price: 0.73, sellUnit: "can")]
        super.setUp()
    }
    
    func testShoppingCart() {
        
        let shoppingCartViewModel = ShoppingCartViewModel(productList: self.defaultProductList)
        XCTAssertEqual(shoppingCartViewModel.numberOfProducts, 4)
        XCTAssertEqual(shoppingCartViewModel.numberOfSections, 1)
        XCTAssertEqual(shoppingCartViewModel.totalAmount, 0)
        
        let milkIndexPath = IndexPath(row: 2, section: 0)
        XCTAssertNotNil(shoppingCartViewModel.productViewModel(at: milkIndexPath))
        
        let invalidIndexPath = IndexPath(row: 9, section: 0)
        XCTAssertNil(shoppingCartViewModel.productViewModel(at: invalidIndexPath))
    }
    
    func testEmptyShoppingCart() {
        let shoppingCartViewModel = ShoppingCartViewModel(productList: [])
        XCTAssertEqual(shoppingCartViewModel.numberOfProducts, 0)
        XCTAssertEqual(shoppingCartViewModel.numberOfSections, 1)
        XCTAssertEqual(shoppingCartViewModel.totalAmount, 0)
    }
    
    func testAddingItemsToShoppingCart() {
        let delegateTester = ShoppingCartDelegateTester()
        let shoppingCartViewModel = ShoppingCartViewModel(productList: self.defaultProductList)
        shoppingCartViewModel.delegate = delegateTester
        
        let milkIndexPath = IndexPath(row: 2, section: 0)
        shoppingCartViewModel.updateQuantityForProduct(at: milkIndexPath, with: 3)
        XCTAssertEqual(shoppingCartViewModel.productViewModel(at: milkIndexPath)!.quantity, "3")
        XCTAssertEqual(shoppingCartViewModel.totalAmount, Decimal(3 * 1.3))
        
        XCTAssertEqual(delegateTester.counter, 1)
        
        let beansIndexPath = IndexPath(row: 3, section: 0)
        shoppingCartViewModel.updateQuantityForProduct(at: beansIndexPath, with: 5)
        XCTAssertEqual(shoppingCartViewModel.productViewModel(at: beansIndexPath)!.quantity, "5")
        XCTAssertEqual(shoppingCartViewModel.totalAmount, Decimal(3 * 1.3 + 5 * 0.73))
        
        XCTAssertEqual(delegateTester.counter, 2)
    }
    
}
