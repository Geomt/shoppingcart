//
//  QuoteListViewModelTests.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 10/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import XCTest

@testable import ShoppingCart

struct ValidQuoteProvider: QuoteProvider {
    func fetchData(completionHandler: @escaping ([Quote], QuoteError?) -> Void) {
        let quoteList = [Quote(name: "EUR", value: 1.2), Quote(name: "CHF", value: 1.03)]
        completionHandler(quoteList, nil)
    }
}

struct EmptyQuoteProvider: QuoteProvider {
    func fetchData(completionHandler: @escaping ([Quote], QuoteError?) -> Void) {
        completionHandler([], nil)
    }
}

struct ConversionErrorQuoteProvider: QuoteProvider {
    func fetchData(completionHandler: @escaping ([Quote], QuoteError?) -> Void) {
        completionHandler([], QuoteError.jsonConversion(error: NSError(domain:"", code:0, userInfo:nil)))
    }
}

struct NetworkErrorQuoteProvider: QuoteProvider {
    func fetchData(completionHandler: @escaping ([Quote], QuoteError?) -> Void) {
        completionHandler([], QuoteError.network(error: NSError(domain:"", code:0, userInfo:nil)))
    }
}

class QuoteDelegateTester: QuoteListViewModelDelegate {
    var counter = 0
    
    func quoteListViewModelDidUpdate(_ quoteListViewModel: QuoteListViewModel) {
        counter += 1
    }
}

class QuoteListViewModelTests: XCTestCase {
   
    func testValidData() {
        let delegateTester = QuoteDelegateTester()
        
        let quoteListViewModel = QuoteListViewModel(quoteProvider: ValidQuoteProvider(), defaultAmount: 4.5, delegate: delegateTester)
        
        XCTAssertEqual(delegateTester.counter, 1)
        
        XCTAssertEqual(quoteListViewModel.numberOfQuotes, 2)
        XCTAssertEqual(quoteListViewModel.numberOfSections, 1)
        
        XCTAssertEqual(quoteListViewModel.mainTitle(for: nil), "$: 4.5")
        
        let quoteViewModel = quoteListViewModel.quoteViewModel(at: IndexPath(row: 0, section: 0))
        XCTAssertEqual(quoteListViewModel.mainTitle(for: quoteViewModel), "€: 5.4")
        
        XCTAssertNil(quoteListViewModel.quoteViewModel(at: IndexPath(row: 7, section: 0)))
    }
    
    func testEmptyData() {
        let delegateTester = QuoteDelegateTester()
        
        let quoteListViewModel = QuoteListViewModel(quoteProvider: EmptyQuoteProvider(), defaultAmount: 4.5, delegate: delegateTester)
        
        XCTAssertEqual(delegateTester.counter, 1)
        
        XCTAssertEqual(quoteListViewModel.numberOfQuotes, 0)
        XCTAssertEqual(quoteListViewModel.numberOfSections, 1)
        
        XCTAssertEqual(quoteListViewModel.mainTitle(for: nil), "$: 4.5")
    }
    
    func testConversionError() {
        let delegateTester = QuoteDelegateTester()
        
        let _ = QuoteListViewModel(quoteProvider: ConversionErrorQuoteProvider(), defaultAmount: 4.5, delegate: delegateTester)
        
        XCTAssertEqual(delegateTester.counter, 0)
    }
    
    func testNetworkError() {
        let delegateTester = QuoteDelegateTester()
        
        let _ = QuoteListViewModel(quoteProvider: NetworkErrorQuoteProvider(), defaultAmount: 4.5, delegate: delegateTester)
        
        XCTAssertEqual(delegateTester.counter, 0)
    }
    
}
