//
//  ShoppingCartTests.swift
//  ShoppingCartTests
//
//  Created by Jorge Enrique Salgado Martin on 3/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import XCTest
@testable import ShoppingCart


class ShoppingCartTests: XCTestCase {
    
    func testShoppingCart() {
        let eggs = Product(name: "Eggs", imageName: "egg", price: 5.3, sellUnit: "dozen")
        let apples = Product(name: "Apples", imageName: nil, price: 6.3, sellUnit: "piece")
        
        let shoppingCart = ShoppingCart(productList: [eggs, apples])
        
        XCTAssertEqual(shoppingCart.contents.count, 2)
        XCTAssertEqual(shoppingCart.contents[0].product, eggs)
        XCTAssertEqual(shoppingCart.contents[1].product, apples)
    }
    
}
