//
//  ProductTest.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 10/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import XCTest

@testable import ShoppingCart

class ProductTest: XCTestCase {
    
    func testProduct() {
        let product = Product(name: "Eggs", imageName: "egg", price: 5.3, sellUnit: "dozen")
        
        XCTAssertEqual(product.name, "Eggs")
        XCTAssertEqual(product.imageName, "egg")
        XCTAssertEqual(product.price, 5.3)
        XCTAssertEqual(product.sellUnit, "dozen")
    }
    
    func testNoImageProduct() {
        let noImageProduct = Product(name: "Apples", imageName: nil, price: 6.3, sellUnit: "piece")
        
        XCTAssertEqual(noImageProduct.name, "Apples")
        XCTAssertNil(noImageProduct.imageName)
        XCTAssertEqual(noImageProduct.price, 6.3)
        XCTAssertEqual(noImageProduct.sellUnit, "piece")
    }
}
