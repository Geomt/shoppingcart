//
//  QuoteTest.swift
//  ShoppingCart
//
//  Created by Jorge Enrique Salgado Martin on 10/7/17.
//  Copyright © 2017 None. All rights reserved.
//

import XCTest

@testable import ShoppingCart

class QuoteTest: XCTestCase {
    
    func testQuote() {
        let quote = Quote(name: "EUR", value: 98.3)
        
        XCTAssertEqual(quote.name
            , "EUR")
        XCTAssertEqual(quote.value, 98.3)
    }
}
